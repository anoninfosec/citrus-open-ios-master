//
//  WalletViewController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import "MerchantConstants.h"
#import "WalletViewController.h"
#import "TestParams.h"
#import "UIUtility.h"

@interface WalletViewController ()

@end

@implementation WalletViewController
@synthesize authLayer,proifleLayer,paymentLayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    //sdk initialization
    [self initializeLayers];
    self.title = @"Citrus iOS SDK";
    [self sampleDataInit];
    
}

-(void)sampleDataInit{
    
    contactInfo = [[CTSContactUpdate alloc] init];
    contactInfo.firstName = TEST_FIRST_NAME;
    contactInfo.lastName = TEST_LAST_NAME;
    contactInfo.email = TEST_EMAIL;
    contactInfo.mobile = TEST_MOBILE;
    
    addressInfo = [[CTSUserAddress alloc] init];
    addressInfo.city = @"Mumbai";
    addressInfo.country = @"India";
    addressInfo.state = @"Maharashtra";
    addressInfo.street1 = @"Golden Road";
    addressInfo.street2 = @"Pink City";
    addressInfo.zip = @"401209";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)initializeLayers{
    //sdk initialization
    CTSKeyStore *keyStore = [[CTSKeyStore alloc] init];
    keyStore.signinId = SignInId;
    keyStore.signinSecret = SignInSecretKey;
    keyStore.signUpId = SubscriptionId;
    keyStore.signUpSecret = SubscriptionSecretKey;
    keyStore.vanity = VanityUrl;
    
    authLayer = [CitrusPaymentSDK fetchSharedAuthLayer];
    proifleLayer = [CitrusPaymentSDK fetchSharedProfileLayer];
    paymentLayer = [CitrusPaymentSDK fetchSharedPaymentLayer];
}

-(IBAction)getWalletBalance{
    [proifleLayer requestGetBalance:^(CTSAmount *amount, NSError *error) {
        LogTrace(@" value %@ ",amount.value);
        LogTrace(@" currency %@ ",amount.currency);
        if (error) {
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Balance is %@ %@",amount.value,amount.currency]];
        }
    }];
}

-(IBAction)recahrgeWalletCreditCard{
    // Credit card
    CTSPaymentDetailUpdate *creditCardInfo = [[CTSPaymentDetailUpdate alloc] init];
    // Update card for card payment.
    CTSElectronicCardUpdate *creditCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
    creditCard.number = TEST_CREDIT_CARD_NUMBER;
    creditCard.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    creditCard.scheme = [CTSUtility fetchCardSchemeForCardNumber:creditCard.number];
    creditCard.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    creditCard.cvv = TEST_CREDIT_CARD_CVV;
    
    [creditCardInfo addCard:creditCard];
    //Vikas LoadWalletReturnUrl -> ReturnUrl
    [paymentLayer requestLoadMoneyInCitrusPay:creditCardInfo withContact:contactInfo withAddress:addressInfo amount:@"100" returnUrl:LoadWalletReturnUrl customParams:nil  returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            LogTrace(@" isAnyoneSignedIn %d",[authLayer isLoggedIn]);
            
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Load Money Status %@",[citrusCashResponse.responseDict valueForKey:LoadMoneyResponeKey]]];
        }
    }];

}
-(IBAction)recahrgeWalletDebitCard{
    // Credit card
     CTSPaymentDetailUpdate *creditCardInfo = [[CTSPaymentDetailUpdate alloc] init];
    // Update card for card payment.
    CTSElectronicCardUpdate *creditCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
    creditCard.number = TEST_CREDIT_CARD_NUMBER;
    creditCard.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    creditCard.scheme = [CTSUtility fetchCardSchemeForCardNumber:creditCard.number];
    creditCard.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    creditCard.cvv = TEST_CREDIT_CARD_CVV;
    
    [creditCardInfo addCard:creditCard];
    [paymentLayer requestLoadMoneyInCitrusPay:creditCardInfo withContact:contactInfo withAddress:addressInfo amount:@"15" returnUrl:LoadWalletReturnUrl customParams:nil  returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            LogTrace(@" isAnyoneSignedIn %d",[authLayer isLoggedIn]);
            
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Load Money Status %@",[citrusCashResponse.responseDict valueForKey:LoadMoneyResponeKey]]];
        }
    }];
}



-(IBAction)recahrgeWalletSavedCard{
    // Card token
   
    [proifleLayer requestPaymentInformationWithCompletionHandler:^(CTSProfilePaymentRes *paymentInfo, NSError *error) {
        if (error == nil) {
            if([paymentInfo.paymentOptions count]){
                //delete saved card
                
                CTSPaymentDetailUpdate *tokenizedCardInfo = [[CTSPaymentDetailUpdate alloc] init];
                // Update card for tokenized payment.
                CTSPaymentOption *card = [paymentInfo.paymentOptions objectAtIndex:0];
                CTSElectronicCardUpdate *tokenizedCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
                tokenizedCard.cvv= TEST_CREDIT_CARD_CVV;
                tokenizedCard.token= card.token;
                [tokenizedCardInfo addCard:tokenizedCard];

                if([card canDoOneTapPayment]){
                    LogTrace(@"CVV found, not sending");
                    tokenizedCard.cvv= nil;
                }
                else
                {
                    tokenizedCard.cvv= TEST_CREDIT_CARD_CVV;
                }

                
                [paymentLayer requestLoadMoneyInCitrusPay:tokenizedCardInfo withContact:contactInfo withAddress:addressInfo amount:@"100" returnUrl:LoadWalletReturnUrl customParams:nil  returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
                    if(error){
                        [UIUtility toastMessageOnScreen:error.localizedDescription];
                    }
                    else {
                        [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Load Money Status %@",[citrusCashResponse.responseDict valueForKey:LoadMoneyResponeKey]]];
                    }
                }];

                
            }
            else{
                [UIUtility toastMessageOnScreen:@" no saved cards found, please save card first"];
            }
        } else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't fetch saved card for deletion \nerror: %@",[error localizedDescription]]];
        }
    }];

    
    
    
    
    

}
-(IBAction)recahrgeWalletNetbanking{
    // Net Banking
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    // Update bank details for net banking payment.
    CTSNetBankingUpdate* netBank = [[CTSNetBankingUpdate alloc] init];
    netBank.code = TEST_NETBAK_CODE;
    [paymentInfo addNetBanking:netBank];
    //Vikas LoadWalletReturnUrl -> ReturnUrl
    [paymentLayer requestLoadMoneyInCitrusPay:paymentInfo withContact:contactInfo withAddress:addressInfo amount:@"100" returnUrl:LoadWalletReturnUrl customParams:nil returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Load Money Status %@",[citrusCashResponse.responseDict valueForKey:LoadMoneyResponeKey]]];
        }
    }];
}

-(IBAction)walletPayment{
    CTSBill *bill = [WalletViewController getBillFromServer:@"1.00"];

    [paymentLayer requestChargeCitrusWalletWithContact:contactInfo address:addressInfo bill:bill returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        LogTrace(@"citrusCashResponse %@",citrusCashResponse);
        LogTrace(@"error %@",error);
        if(error){
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            LogTrace(@" isAnyoneSignedIn %d",[authLayer isLoggedIn]);
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" transaction complete\n txStatus: %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
        }
    }];
}

-(IBAction)sendMoney{
    [paymentLayer requestTransferMoneyTo:TEST_MOBILE amount:@"1.23" message:@"Here is Some Money for you" completionHandler:^(CTSTransferMoneyResponse*transferMoneyRes,  NSError *error) {
        LogTrace(@" transferMoneyRes %@ ",transferMoneyRes);
        
        LogTrace(@" error %@ ",[error localizedDescription]);
        
        if (error) {
            
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Transfer Status %@",transferMoneyRes.status]];
        }
    }];

}



+ (CTSBill*)getBillFromServer:(NSString *)amount{
    // Configure your request here.
    NSMutableURLRequest* urlReq = [[NSMutableURLRequest alloc] initWithURL:
                                   [NSURL URLWithString:[NSString stringWithFormat:@"%@?amount=%@",BillUrl,amount]]];
    [urlReq setHTTPMethod:@"POST"];
    NSError* error = nil;
    NSData* signatureData = [NSURLConnection sendSynchronousRequest:urlReq
                                                  returningResponse:nil
                                                              error:&error];
    NSString* billJson = [[NSString alloc] initWithData:signatureData
                                               encoding:NSUTF8StringEncoding];
    JSONModelError *jsonError;
    CTSBill* sampleBill = [[CTSBill alloc] initWithString:billJson
                                                    error:&jsonError];
    LogTrace(@"billJson %@",billJson);
    LogTrace(@"signature %@ ", sampleBill);
    return sampleBill;
    
}


//-(IBAction)accessConsumerPortal{
//    [authLayer accessConsumerPortalWithParentViewController:self withCompletionHandler:^(NSError *error) {
//        NSString *toastMessage;
//        if (error) {
//            LogDebug(@" error:%@", error.localizedDescription);
//            toastMessage = error.localizedDescription;
//        }
//        else{
//            LogDebug(@"Consumer Portal will load shortly");
//            toastMessage = @"Consumer Portal will load shortly";
//        }
//        [UIUtility toastMessageOnScreen:toastMessage];
//    }];
//}

@end
