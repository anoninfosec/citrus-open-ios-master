//
//  DynamicPricingViewController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//
#import "MerchantConstants.h"
#import "DynamicPricingViewController.h"
#import "TestParams.h"
#import "UIUtility.h"
#define RULE_NAME @"YaddyBoy10"

@interface DynamicPricingViewController ()

@end

@implementation DynamicPricingViewController
@synthesize paymentLayer,profileLayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self sampleDataInit];
    [self grabLayers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)grabLayers{
    paymentLayer = [CitrusPaymentSDK fetchSharedPaymentLayer];
    profileLayer = [CitrusPaymentSDK fetchSharedProfileLayer];
}

-(void)sampleDataInit{
    
    contactInfo = [[CTSContactUpdate alloc] init];
    contactInfo.firstName = TEST_FIRST_NAME;
    contactInfo.lastName = TEST_LAST_NAME;
    contactInfo.email = TEST_EMAIL;
    contactInfo.mobile = TEST_MOBILE;
    
    addressInfo = [[CTSUserAddress alloc] init];
    addressInfo.city = @"Mumbai";
    addressInfo.country = @"India";
    addressInfo.state = @"Maharashtra";
    addressInfo.street1 = @"Golden Road";
    addressInfo.street2 = @"Pink City";
    addressInfo.zip = @"401209";
}


-(IBAction)reqeustDpValidate{
    
    
    CTSElectronicCardUpdate *instrument = [[CTSElectronicCardUpdate alloc] initCreditCard];
    instrument.number = TEST_CREDIT_CARD_NUMBER;
    instrument.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    instrument.scheme = [CTSUtility fetchCardSchemeForCardNumber:instrument.number];
    instrument.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    instrument.cvv = TEST_CREDIT_CARD_CVV;
    
    CTSRuleInfo *ruleInfo = [[CTSRuleInfo alloc] init];
    ruleInfo.ruleName = RULE_NAME;
    ruleInfo.alteredAmount = @"80.00";
    ruleInfo.originalAmount = @"100";
    ruleInfo.operationType = DPRequestTypeValidate;

    
    
    
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    [paymentInfo addCard:instrument];
    
    CTSUser *user = [[CTSUser alloc] init];
    user.email = TEST_EMAIL;
    user.mobile = TEST_MOBILE;
    
    
    [paymentLayer requestPerformDynamicPricingRule:ruleInfo paymentInfo:paymentInfo billUrl:BillUrl user:user extraParams:nil completionHandler:^(CTSDyPResponse *dyPResponse, NSError *error) {
        LogTrace(@"error %@",error);
        LogTrace(@"dyPResponse %@",dyPResponse);
        
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:  [NSString stringWithFormat:@"Request Status: %@\nOriginal Amount: %@\n Altered amount: %@",dyPResponse.resultMessage,dyPResponse.originalAmount.value,dyPResponse.alteredAmount.value] ];
        }
    }];

}
-(IBAction)reqeustDpSearchAndApply{
    
    CTSElectronicCardUpdate *instrument = [[CTSElectronicCardUpdate alloc] initCreditCard];
    instrument.number = TEST_CREDIT_CARD_NUMBER;
    instrument.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    instrument.scheme = [CTSUtility fetchCardSchemeForCardNumber:instrument.number];
    instrument.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    instrument.cvv = TEST_CREDIT_CARD_CVV;
    
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    [paymentInfo addCard:instrument];
    
    CTSRuleInfo *ruleInfo = [[CTSRuleInfo alloc] init];
    ruleInfo.originalAmount = @"100";
    ruleInfo.operationType = DPRequestTypeSearchAndApply;
    
    
    CTSUser *user = [[CTSUser alloc] init];
    user.email = TEST_EMAIL;
    user.mobile = TEST_MOBILE;
    
    
    [paymentLayer requestPerformDynamicPricingRule:ruleInfo paymentInfo:paymentInfo billUrl:BillUrl user:user extraParams:nil completionHandler:^(CTSDyPResponse *dyPResponse, NSError *error) {
        LogTrace(@"error %@",error);
        LogTrace(@"dyPResponse %@",dyPResponse);
        
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:  [NSString stringWithFormat:@"Request Status: %@\nOriginal Amount: %@\n Altered amount: %@",dyPResponse.resultMessage,dyPResponse.originalAmount.value,dyPResponse.alteredAmount.value] ];
        }
    }];

}

-(IBAction)reqeustDpCalculate{
    
    CTSElectronicCardUpdate *instrument = [[CTSElectronicCardUpdate alloc] initCreditCard];
    instrument.number = TEST_CREDIT_CARD_NUMBER;
    instrument.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    instrument.scheme = [CTSUtility fetchCardSchemeForCardNumber:instrument.number];
    instrument.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    instrument.cvv = TEST_CREDIT_CARD_CVV;
    
    CTSRuleInfo *ruleInfo = [[CTSRuleInfo alloc] init];
    ruleInfo.ruleName = RULE_NAME;
    ruleInfo.originalAmount = @"100";
    ruleInfo.operationType = DPRequestTypeCalculate;
    
    
    //    CTSElectronicCardUpdate *instrument = [[CTSElectronicCardUpdate alloc] initCreditCard];
    //    instrument.cvv= TEST_CREDIT_CARD_CVV;
    //    instrument.token= @"2ecd7e218f1663d2c9285e54994eec43";
    
    
    
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    [paymentInfo addCard:instrument];
    
    CTSUser *user = [[CTSUser alloc] init];
    user.email = TEST_EMAIL;
    user.mobile = TEST_MOBILE;
    
    
    [paymentLayer requestPerformDynamicPricingRule:ruleInfo paymentInfo:paymentInfo billUrl:BillUrl user:user extraParams:nil completionHandler:^(CTSDyPResponse *dyPResponse, NSError *error) {
        LogTrace(@"error %@",error);
        LogTrace(@"dyPResponse %@",dyPResponse);
        
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:  [NSString stringWithFormat:@"Request Status: %@\nOriginal Amount: %@\n Altered amount: %@",dyPResponse.resultMessage,dyPResponse.originalAmount.value,dyPResponse.alteredAmount.value] ];
        }
    }];

}


-(IBAction)payUsingDynamicPricing{
    [paymentLayer requestChargeDynamicPricingContact:contactInfo withAddress:addressInfo customParams:nil returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        NSLog(@"citrusCashResponse %@",citrusCashResponse);
        NSLog(@"error %@",error);
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
        }
    }];
}

-(IBAction)merchantsRules{

    CTSDPMerchantQueryReq *dpMerchantQuery = [[CTSDPMerchantQueryReq alloc] init];
    dpMerchantQuery.merchantAccessKey = @"M9OZT7LFHPCK91UDVJC8";
    dpMerchantQuery.signature = @"ef1e242bd0c8f8d3de194467d96a32c5220c6917";
    
    [profileLayer requestDpMerchantQuery:dpMerchantQuery completionHandler:^(CTSDPResponse *reponse, NSError *error) {
        LogTrace(@"reponse %@",reponse);
        LogTrace(@"error %@",error);
        
        
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Found Rules:\n %@",reponse.extraParams]];
        }
    }];
}



@end
