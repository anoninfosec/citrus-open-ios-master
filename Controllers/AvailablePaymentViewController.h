//
//  AvailablePaymentViewController.h
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/9/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitrusSdk.h"
@interface AvailablePaymentViewController : UIViewController
@property(strong)CTSPaymentLayer *paymentLayer;
@end
