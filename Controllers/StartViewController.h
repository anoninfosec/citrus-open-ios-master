//
//  StartViewController.h
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface StartViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *_tblView;
}

@end
