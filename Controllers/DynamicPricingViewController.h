//
//  DynamicPricingViewController.h
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitrusSdk.h"

@interface DynamicPricingViewController : UIViewController{
    CTSContactUpdate* contactInfo;
    CTSUserAddress* addressInfo;
}
@property(strong)CTSPaymentLayer *paymentLayer;
@property(strong)CTSProfileLayer *profileLayer;


@end
