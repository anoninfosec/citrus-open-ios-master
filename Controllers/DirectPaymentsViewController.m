//
//  DirectPaymentsViewController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import "DirectPaymentsViewController.h"
#import "TestParams.h"
#import "UIUtility.h"
#import "MerchantConstants.h"
#define toErrorDescription(error) [error.userInfo objectForKey:NSLocalizedDescriptionKey]
@interface DirectPaymentsViewController ()

@end

@implementation DirectPaymentsViewController
@synthesize profileLayer,paymentLayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeLayers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeLayers{
    profileLayer = [CitrusPaymentSDK fetchSharedProfileLayer];
    paymentLayer = [CitrusPaymentSDK fetchSharedPaymentLayer];
}

-(IBAction)payUsingCreditCard{
    // Update card for card payment.
    CTSElectronicCardUpdate *creditCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
    creditCard.number = TEST_CREDIT_CARD_NUMBER;
    creditCard.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    creditCard.scheme = [CTSUtility fetchCardSchemeForCardNumber:creditCard.number];
    creditCard.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    creditCard.cvv = TEST_CREDIT_CARD_CVV;
    
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    [paymentInfo addCard:creditCard];
    
    //Bill fetch, can be removed and replaced with your own implementation
    [CTSUtility requestBillAmount:@"1" billURL:BillUrl callback:^(CTSBill *bill, NSError *error) {
        if(error == nil){
            [paymentLayer requestChargePayment:paymentInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:nil returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
                if(error){
                    [UIUtility toastMessageOnScreen:error.localizedDescription];
                }
                else {
                    [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
                }
            }];
        }
        else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Bill Fetch Error : %@",error.localizedDescription]];
        }
    }];
}


-(IBAction)payUsingDebitCard{
    // Update card for card payment.
    CTSElectronicCardUpdate *creditCard = [[CTSElectronicCardUpdate alloc] initDebitCard];
    creditCard.number = TEST_CREDIT_CARD_NUMBER;
    creditCard.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
    creditCard.scheme = [CTSUtility fetchCardSchemeForCardNumber:creditCard.number];
    creditCard.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
    creditCard.cvv = nil;
    
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    [paymentInfo addCard:creditCard];
    
    // Get your bill here.
    
    [CTSUtility requestBillAmount:@"1" billURL:BillUrl callback:^(CTSBill *bill, NSError *error) {
        if(error == nil){
            
            [paymentLayer requestChargePayment:paymentInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:@{@"Test":@"cust"} returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
                if(error){
                    [UIUtility toastMessageOnScreen:error.localizedDescription];
                }
                else {
                    [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
                }
            }];
            
        }else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Bill Fetch Error : %@",error.localizedDescription]];
        }
    }];
    
}
-(IBAction)payUsingNetBanking{
    
    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    // Update bank details for net banking payment.
    CTSNetBankingUpdate* netBank = [[CTSNetBankingUpdate alloc] init];
    netBank.code = @"CID002";
    //netBank.name = @"NAME";
    [paymentInfo addNetBanking:netBank];
    
    // Get your bill here.
    [CTSUtility requestBillAmount:@"1" billURL:BillUrl callback:^(CTSBill *bill, NSError *error) {
        if(error == nil){
            [paymentLayer requestChargePayment:paymentInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:customParams returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
                if(error){
                    [UIUtility toastMessageOnScreen:error.localizedDescription];
                }
                else {
                    [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
                }
            }];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Bill Fetch Error : %@",error.localizedDescription]];
        }
    }];
}



-(IBAction)payUsingSavedCard{
    
    [profileLayer requestPaymentInformationWithCompletionHandler:^(CTSProfilePaymentRes *paymentInfo, NSError *error) {
        if (error == nil) {
            NSMutableString *toastString = [[NSMutableString alloc] init];
            if([paymentInfo.paymentOptions count]){
                CTSPaymentOption *paymentOption = [paymentInfo.paymentOptions objectAtIndex:0];
                
                CTSPaymentDetailUpdate *tokenizedCardInfo = [[CTSPaymentDetailUpdate alloc] init];
                CTSElectronicCardUpdate *tokenizedCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
                
                
                if([paymentOption canDoOneTapPayment]){
                 LogTrace(@"CVV found, not sending");
                  tokenizedCard.cvv= nil;
                }
                else
                {
                    tokenizedCard.cvv= TEST_CREDIT_CARD_CVV;
                }
                
                tokenizedCard.token= paymentOption.token;
                [tokenizedCardInfo addCard:tokenizedCard];
                
                // Get your bill here.
                                 [CTSUtility requestBillAmount:@"1" billURL:BillUrl callback:^(CTSBill *bill, NSError *error) {
            
                [paymentLayer requestChargePayment:tokenizedCardInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:customParams returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
                    if(error){
                        [UIUtility toastMessageOnScreen:error.localizedDescription];
                    }
                    else {
                        [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
                    }
                }];
                     
            }
                  ];
            }
            else{
                toastString =(NSMutableString *) @" no saved cards found, please save card first";
                [UIUtility toastMessageOnScreen:toastString];
            }
        } else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't fetch saved card for payment \nerror: %@",[error localizedDescription]]];
        }
    }];

}


+ (CTSBill*)getBillFromServer:(NSString *)amount{
    // Configure your request here.
    NSMutableURLRequest* urlReq = [[NSMutableURLRequest alloc] initWithURL:
                                   [NSURL URLWithString:[NSString stringWithFormat:@"%@?amount=%@",BillUrl,amount]]];
    [urlReq setHTTPMethod:@"POST"];
    NSError* error = nil;
    NSData* signatureData = [NSURLConnection sendSynchronousRequest:urlReq
                                                  returningResponse:nil
                                                              error:&error];
    NSString* billJson = [[NSString alloc] initWithData:signatureData
                                               encoding:NSUTF8StringEncoding];
    JSONModelError *jsonError;
    CTSBill* sampleBill = [[CTSBill alloc] initWithString:billJson
                                                    error:&jsonError];
    LogTrace(@"billJson %@",billJson);
    LogTrace(@"signature %@ ", sampleBill);
    return sampleBill;
    
}


- (IBAction)getPGHealth{
    [paymentLayer requestGetPGHealthWithCompletionHandler:^(CTSPGHealthRes *pgHealthRes,
                                                            NSError *error) {
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"pgHealthRes %@",pgHealthRes.responseDict]];
        }
    }];
}


@end
