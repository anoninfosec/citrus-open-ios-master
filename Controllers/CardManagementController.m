//
//  CardManagementController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import "CardManagementController.h"
#import "TestParams.h"
#import "UIUtility.h"
#define toErrorDescription(error) [error.userInfo objectForKey:NSLocalizedDescriptionKey]

@interface CardManagementController ()

@end

@implementation CardManagementController
@synthesize authLayer,proifleLayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeLayers];
}


#pragma mark - initializers

// Initialize the SDK layer viz CTSAuthLayer/CTSProfileLayer/CTSPaymentLayer
-(void)initializeLayers{
    authLayer = [CitrusPaymentSDK fetchSharedAuthLayer];
    proifleLayer = [CitrusPaymentSDK fetchSharedProfileLayer];
}

-(IBAction)bindUser{
    [authLayer requestBindUsername:TEST_EMAIL mobile:TEST_MOBILE completionHandler:^(NSString *userName, NSError *error) {
        LogTrace(@"error.code %ld ", (long)error.code);
        
        if(error == nil){
            // Your code to handle success.
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" %@ is now bound",TEST_EMAIL]];
        }
        else {
            // Your code to handle error.
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't bind %@\nerror: %@",TEST_EMAIL,[error localizedDescription]]];
        }
    }];
}

-(IBAction)saveCard{
    
    
        
        CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
        // Credit card info for card payment type.
        CTSElectronicCardUpdate *creditCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
        creditCard.number = TEST_CREDIT_CARD_NUMBER;
        creditCard.expiryDate = TEST_CREDIT_CARD_EXPIRY_DATE;
        creditCard.scheme = [CTSUtility fetchCardSchemeForCardNumber:creditCard.number];
        creditCard.ownerName = TEST_CREDIT_CARD_OWNER_NAME;
        creditCard.name = TEST_CREDIT_CARD_BANK_NAME;
        //creditCard.cvv = TEST_CREDIT_CARD_CVV;
        
        paymentInfo.defaultOption = creditCard.name;
        
        [paymentInfo addCard:creditCard];
        
        
        
        // Configure your request here.
        [proifleLayer updatePaymentInformation:paymentInfo withCompletionHandler:^(NSError *error) {
            if(error == nil){
                // Your code to handle success.
                [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" succesfully card saved \n%@",creditCard.number]];
            }
            else {
                // Your code to handle error.
                [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't save card\n error: %@",toErrorDescription(error)]];
            }
        }];
    
    
}



-(IBAction)saveNetBank{

    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] init];
    // Credit card info for card payment type.
    CTSNetBankingUpdate *bank = [[CTSNetBankingUpdate alloc] init];
    bank.bank = TEST_NETBAK_NAME; //this name has to come from [paymentLayer requestMerchantPgSettings:]
    bank.name = @"Dad's Bank Account";
    paymentInfo.defaultOption = bank.name;
    [paymentInfo addNetBanking:bank];
    
    
    
    // Configure your request here.
    [proifleLayer updatePaymentInformation:paymentInfo withCompletionHandler:^(NSError *error) {
        if(error == nil){
            // Your code to handle success.
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" succesfully saved bank \n%@",bank.name]];
        }
        else {
            // Your code to handle error.
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't save bank\n error: %@",toErrorDescription(error)]];
        }
    }];
}



-(IBAction)getSavedCard{
    [proifleLayer requestPaymentInformationWithCompletionHandler:^(CTSProfilePaymentRes *paymentInfo, NSError *error) {
        if (error == nil) {
            // Your code to handle success.
            
            //Fetching save NetBanking Option
            //NSArray  *netBankingArray = paymentInfo.getSavedNBPaymentOption;
            
            //Fetching save Debit cards Option
            //NSArray  *debitCardArray = paymentInfo.getSavedDCPaymentOption;
            
            //Fetching save Credit cards Option
            //NSArray  *creditCardArray = paymentInfo.getSavedCCPaymentOption;
            
            NSMutableString *toastString = [[NSMutableString alloc] init];
            if([paymentInfo.paymentOptions count]){
                [toastString appendString:[self convertToString:[paymentInfo.paymentOptions objectAtIndex:0]]];
            }
            else{
                toastString =(NSMutableString *) @" no saved cards, please save card first";
            }
            
            CTSPaymentOption *defaultOption = [paymentInfo getDefaultOption];
            if(defaultOption){
                [toastString appendString:[NSString stringWithFormat:@"\n\nDefault \n%@",defaultOption.name]];
            }
            
            [UIUtility toastMessageOnScreen:toastString];
        } else {
            // Your code to handle error.
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't find saved cards \nerror: %@",[error localizedDescription]]];
        }
    }];

}

-(IBAction)deleteCard{
    //get saved cards
    [proifleLayer requestPaymentInformationWithCompletionHandler:^(CTSProfilePaymentRes *paymentInfo, NSError *error) {
        if (error == nil) {
            NSMutableString *toastString = [[NSMutableString alloc] init];
            if([paymentInfo.paymentOptions count]){
                [toastString appendString:[self convertToString:[paymentInfo.paymentOptions objectAtIndex:0]]];
                
                
                
                //delete saved card
                CTSPaymentOption *card = [paymentInfo.paymentOptions objectAtIndex:0];
                
                [proifleLayer requestDeleteCardWithToken:card.token withCompletionHandler:^(NSError *error) {
                    
                    if(!error){
                        [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" Successfully deleted card \n%@",card.number]];
                    }
                    else {
                        [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't delete card \nerror: %@",[error localizedDescription]]];
                        
                    }
                }];
                
            }
            else{
                toastString =(NSMutableString *) @" no saved cards found, please save card first";
                [UIUtility toastMessageOnScreen:toastString];

            }
        } else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't fetch saved card for deletion \nerror: %@",[error localizedDescription]]];
        }
    }];

}

// String parser
-(NSString *)convertToString:(CTSPaymentOption *)option{
    
    NSMutableString *msgString = [[NSMutableString alloc] init];
    if(option.name){
        [msgString appendFormat:@"\n  type: %@",option.type];
    }
    if(option.name){
        [msgString appendFormat:@"\n  name: %@",option.name];
    }
    if(option.owner){
        [msgString appendFormat:@"\n  owner: %@",option.owner];
    }
    if(option.bank){
        [msgString appendFormat:@"\n  bank: %@",option.bank];
    }
    if(option.number){
        [msgString appendFormat:@"\n  number: %@",option.number];
    }
    if(option.expiryDate){
        [msgString appendFormat:@"\n  expiryDate: %@",option.expiryDate];
    }
    if(option.scheme){
        [msgString appendFormat:@"\n  scheme: %@",option.scheme];
    }
    if(option.token){
        [msgString appendFormat:@"\n  token: %@",option.token];
    }
    if(option.mmid){
        [msgString appendFormat:@"\n  mmid: %@",option.mmid];
    }
    if(option.impsRegisteredMobile){
        [msgString appendFormat:@"\n  impsRegisteredMobile: %@",option.impsRegisteredMobile];
    }
    if(option.code){
        [msgString appendFormat:@"\n  code: %@",option.code];
    }
    return msgString;
}

@end
