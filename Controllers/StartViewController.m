//
//  StartViewController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//
#import "MerchantConstants.h"
#import "StartViewController.h"
#import "CardManagementController.h"
#import "WalletViewController.h"
#import "DirectPaymentsViewController.h"
#import "DynamicPricingViewController.h"
#import "CashoutViewController.h"
#import "AvailablePaymentViewController.h"
#import "UserManagementViewController.h"
#import "TestParams.h"
#import "CitrusSdk.h"
#import "CTSCVVEncryptor.h"
@interface StartViewController (){
    NSArray *sdkTasks;

}

@end

@implementation StartViewController

#define SAVE_CARDS @"card management " //bind save delete
#define PAYMENTS_CARDS @"direct payments "//CC, DC, netbank
#define USER_MANAGMENT @"user management"
#define WALLET @"wallet " //link,set password, forgot password, signin, get balance, load using CC,DC, saved, NB, pay using cash, send money
#define DYNAMIC_PRICING @"dynamic pricing " //request and pay
#define WALLET_CASHOUT @"wallet cashout"
#define PAYMENT_OPTIONS @"others . . ."


-(NSDictionary *)getRegistrationDict{
    return @{
             SAVE_CARDS : [[CardManagementController alloc] init],
             PAYMENTS_CARDS : [[DirectPaymentsViewController alloc] init],
             WALLET : [[WalletViewController alloc] init],
             DYNAMIC_PRICING : [[DynamicPricingViewController alloc] init],
             WALLET_CASHOUT : [[CashoutViewController alloc] init],
             PAYMENT_OPTIONS:[[AvailablePaymentViewController alloc]init],
             USER_MANAGMENT:[[UserManagementViewController alloc]init],
             };
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidLoad {
    [super viewDidLoad];
    sdkTasks = [[NSArray alloc] initWithObjects:SAVE_CARDS,PAYMENTS_CARDS,USER_MANAGMENT,WALLET,DYNAMIC_PRICING,WALLET_CASHOUT,PAYMENT_OPTIONS, nil];

    //Citrus SDK initialization
    [self initializeCitrusSdk];
}



-(void)initializeCitrusSdk{
    //sdk initialization
    CTSKeyStore *keyStore = [[CTSKeyStore alloc] init];
    keyStore.signinId = SignInId;
    keyStore.signinSecret = SignInSecretKey;
    keyStore.signUpId = SubscriptionId;
    keyStore.signUpSecret = SubscriptionSecretKey;
    keyStore.vanity = VanityUrl;
    
   
    [CitrusPaymentSDK initializeWithKeyStore:keyStore environment:CTSEnvSandbox];


}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sdkTasks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *_cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:_cellIdentifier];
    }
    
    cell.textLabel.text = [sdkTasks objectAtIndex:[indexPath row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController *viewController = [self controllerForIndex:(int)[indexPath row]];
    if(viewController)
    [self.navigationController pushViewController:viewController animated:YES];
}

-(UIViewController *)controllerForIndex:(int)index{
    UIViewController *controller = nil;
    if([sdkTasks count] - 1 >= index){
        NSString *task = [sdkTasks objectAtIndex:index];
        controller = [[self getRegistrationDict] valueForKey:task];
    
    
    }
    return controller;
}



@end
