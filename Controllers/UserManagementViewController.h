//
//  UserManagementViewController.h
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/9/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitrusSdk.h"
@interface UserManagementViewController : UIViewController
@property(strong)CTSProfileLayer *proifleLayer;
@property(strong)CTSAuthLayer *authLayer;
@property(strong)CTSPaymentLayer *paymentLayer;
@property (strong, nonatomic) IBOutlet UITextField *otp;

@end
