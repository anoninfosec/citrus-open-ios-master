//
//  AvailablePaymentViewController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/9/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import "MerchantConstants.h"
#import "AvailablePaymentViewController.h"
#import "UIUtility.h"
@interface AvailablePaymentViewController ()

@end

@implementation AvailablePaymentViewController
@synthesize paymentLayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    paymentLayer = [CitrusPaymentSDK fetchSharedPaymentLayer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)fetchAvailablePaymentOptions{
    [paymentLayer requestMerchantPgSettings:VanityUrl withCompletionHandler:^(CTSPgSettings *pgSettings, NSError *error) {
        if(error){
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else {
            LogTrace(@" pgSettings %@ ", pgSettings);
            NSMutableString *responseString = [[NSMutableString alloc] init];
            
            [responseString appendString:@"Credit Cards:\n"];

            for (NSString* val in pgSettings.creditCard) {
                LogTrace(@"CC %@ ", val);
                [responseString appendString:[NSString stringWithFormat:@"\n%@",val]];

            }
            
            [responseString appendString:@"\nDebit Cards:\n"];
            for (NSString* val in pgSettings.debitCard) {
                [responseString appendString:[NSString stringWithFormat:@"\n%@",val]];
            }
            
            [responseString appendString:@"\nNetBanking:\n"];

            
            for (NSDictionary* arr in pgSettings.netBanking) {
                LogTrace(@"bankName %@ ", [arr valueForKey:@"bankName"]);
                LogTrace(@"issuerCode %@ ", [arr valueForKey:@"issuerCode"]);
                
                [responseString appendString:[NSString stringWithFormat:@"\n%@, %@",[arr valueForKey:@"bankName"],[arr valueForKey:@"issuerCode"]]];
            }
            [UIUtility toastMessageOnScreen:responseString];
        }
        
    }];

}


-(IBAction)fetchLoadmoneyPaymentOptions{

    [paymentLayer requestLoadMoneyPgSettingsCompletionHandler:^(CTSPgSettings *pgSettings, NSError *error){
        if(error){
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else {
            LogTrace(@" pgSettings %@ ", pgSettings);
            NSMutableString *responseString = [[NSMutableString alloc] init];
            
            [responseString appendString:@"Credit Cards:\n"];
            
            for (NSString* val in pgSettings.creditCard) {
                LogTrace(@"CC %@ ", val);
                [responseString appendString:[NSString stringWithFormat:@"\n%@",val]];
                
            }
            
            [responseString appendString:@"\nDebit Cards:\n"];
            for (NSString* val in pgSettings.debitCard) {
                [responseString appendString:[NSString stringWithFormat:@"\n%@",val]];
            }
            
            [responseString appendString:@"\nNetBanking:\n"];
            
            
            for (NSDictionary* arr in pgSettings.netBanking) {
                LogTrace(@"bankName %@ ", [arr valueForKey:@"bankName"]);
                LogTrace(@"issuerCode %@ ", [arr valueForKey:@"issuerCode"]);
                
                [responseString appendString:[NSString stringWithFormat:@"\n%@, %@",[arr valueForKey:@"bankName"],[arr valueForKey:@"issuerCode"]]];
            }
            [UIUtility toastMessageOnScreen:responseString];
        }
        
    }];


}

-(IBAction)binService{
    
 //service takes first six digits of the card
    
[paymentLayer requestCardDetails:@"524508" completionHandler:^(CTSCardBinResponse *cardBinResponse, NSError *error) {
    if(error){
        [UIUtility toastMessageOnScreen:[error localizedDescription]];
    }
    else {
        [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"type : %@\nscheme : %@\ncountry : %@\nissuing bank : %@ \n",cardBinResponse.cardtype,cardBinResponse.cardscheme,cardBinResponse.country,cardBinResponse.issuingbank]];
    }
}];

}


@end
