//
//  CashoutViewController.h
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/9/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitrusSdk.h"

@interface CashoutViewController : UIViewController
@property(strong)CTSAuthLayer *authLayer;
@property(strong)CTSProfileLayer *profileLayer;
@property(strong)CTSPaymentLayer *paymentLayer;
@end
