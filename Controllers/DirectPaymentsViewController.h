//
//  DirectPaymentsViewController.h
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/8/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitrusSdk.h"

@interface DirectPaymentsViewController : UIViewController{
   CTSContactUpdate* contactInfo;
   CTSUserAddress* addressInfo;
   NSDictionary *customParams;
}

@property(strong)CTSProfileLayer *profileLayer;
@property(strong)CTSPaymentLayer *paymentLayer;

@end
