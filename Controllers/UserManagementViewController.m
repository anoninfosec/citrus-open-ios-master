//
//  UserManagementViewController.m
//  CTS iOS Sdk
//
//  Created by Yadnesh on 10/9/15.
//  Copyright © 2015 Citrus. All rights reserved.
//

#import "UserManagementViewController.h"
#import "UIUtility.h"
#import "TestParams.h"
@interface UserManagementViewController ()

@end

@implementation UserManagementViewController
@synthesize authLayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    authLayer = [CitrusPaymentSDK fetchSharedAuthLayer];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)linkUser{
    [authLayer requestCitrusLink:TEST_EMAIL mobile:TEST_MOBILE completion:^(CTSCitrusLinkRes *linkResponse, NSError *error) {
        if (error) {
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            [UIUtility toastMessageOnScreen:linkResponse.userMessage];
            
            switch (linkResponse.siginType) {
                case CitrusSiginTypeMOtpOrPassword:
                    // Show Mobile otp and password sign in screen
                    break;
                case CitrusSiginTypeMOtp:
                    // Show Mobile otp sign in screen
                    break;
                case CitrusSiginTypeEOtpOrPassword:
                    // Show Email otp and password sign in screen
                    break;
                case CitrusSiginTypeEOtp:
                    // Show Email otp sign in screen
                    break;
                default:
                    break;
            }
         }
    }];
    
    

    
    
    
    
}

-(IBAction)forgotPassword{
    
    [authLayer requestResetPassword:TEST_EMAIL completionHandler:^(NSError *error) {
        if (error) {
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"reset link sent to email address"]];
        }
    }];
}


-(IBAction)isLoggedIn{
    if([authLayer isLoggedIn]){
        [UIUtility toastMessageOnScreen:@"user is signed in"];
    }
    else{
        [UIUtility toastMessageOnScreen:@"no one is logged in"];
    }
}

-(IBAction)signout{
    [authLayer signOut];
    [UIUtility toastMessageOnScreen:@"sign out successful"];
}



-(IBAction)signin{
    [authLayer requestCitrusLinkSignInWithPassoword:TEST_PASSWORD passwordType:PasswordTypePassword completionHandler:^(NSError *error) {
        if (error) {
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"You are now logged in"]];
        }
    }];
    
}

-(IBAction)signinOtp{
    [authLayer requestCitrusLinkSignInWithPassoword:self.otp.text passwordType:PasswordTypeOtp completionHandler:^(NSError *error) {
        LogTrace(@"error %@",error);
        if (error) {
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"You are now logged in"]];
        }
    }];
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    LogTrace(@"You entered %@",self.otp.text);
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    LogTrace(@"You entered %@",self.otp.text);
    [textField resignFirstResponder];
}



@end
