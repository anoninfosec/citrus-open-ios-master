//
//  CardsViewController.h
//  PaymentSdk_GUI
//
//  Created by Vikas Singh on 8/26/15.
//  Copyright (c) 2015 Vikas Singh. All rights reserved.
//

#import "BaseViewController.h"

@interface CardsViewController : BaseViewController

@property (nonatomic , strong) NSString *amount;
@property (assign) int landingScreen;

@property (nonatomic , weak) IBOutlet UITextField *cardNumberTextField;
@property (nonatomic , weak) IBOutlet UITextField *ownerNameTextField;
@property (nonatomic , weak) IBOutlet UITextField *expiryMonthTextField;
@property (nonatomic , weak) IBOutlet UITextField *expiryYearTextField;
@property (nonatomic , weak) IBOutlet UITextField *schemeTextField;
@property (nonatomic , weak) IBOutlet UITextField *cvvTextField;
@property (nonatomic , weak) IBOutlet UITextField *netBankCodeTextField;
@property (nonatomic , weak) IBOutlet UIImageView *schemeTypeImageView;
@property (nonatomic , weak) IBOutlet UISegmentedControl *segmentedControl;

@property (nonatomic , weak) IBOutlet UITextField *expiryDateTextField;


@property (nonatomic , weak) IBOutlet UIView *cardView;
@property (nonatomic , weak) IBOutlet UIView *netBankingView;
@property (nonatomic , weak) IBOutlet UITableView *saveCardsTableView;

@property (nonatomic , weak) IBOutlet UIButton *loadButton;
@property (nonatomic , weak) IBOutlet UIButton *loadMoneyButton;
@property (nonatomic , weak) IBOutlet UIButton *saveCardButton;

@property (nonatomic , weak) IBOutlet UIPickerView *pickerView;

@property (nonatomic , weak) IBOutlet UIActivityIndicatorView *indicatorView;

@property (nonatomic , strong) CTSRuleInfo *ruleInfo;
@property (assign) DPRequestType dpType;


@end
