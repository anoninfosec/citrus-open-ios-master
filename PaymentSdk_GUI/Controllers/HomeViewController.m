//
//  HomeViewController.m
//  PaymentSdk_GUI
//
//  Created by Vikas Singh on 8/26/15.
//  Copyright (c) 2015 Vikas Singh. All rights reserved.
//

#import "HomeViewController.h"
#import "UIUtility.h"
#import "CardsViewController.h"


@interface HomeViewController (){

    int option;
    UIAlertView *alert;
    CGRect frame;
    NSArray *array;
    int selectedRule;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetting];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self resetUI];
    self.transparentViewView.frame = CGRectMake(0, self.view.bounds.size.height+100, self.view.bounds.size.width, self.view.bounds.size.height);
    frame = self.transparentViewView.frame;
    [self getBalance:nil];
}

- (void) viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    self.transparentViewView.hidden = TRUE;
}

#pragma mark - Initial Setting Methods
- (void) initialSetting{

    [self.navigationItem setHidesBackButton:YES];
    
    array =[NSArray arrayWithObjects:@"Search and Apply Rule",@"Apply Coupon or Rule",@"Validate Rule", nil];
    self.containerView.layer.cornerRadius = 4;
    self.indicatorView.hidden = TRUE;
    self.nameLabel.text = self.userName;
    self.title = @"Home";
    self.cancelButton.layer.cornerRadius = 4;
    self.applyButton.layer.cornerRadius = 4;
//    self.getAmountButton.layer.cornerRadius = 4;
//    
//    self.sendMoneyButton.layer.cornerRadius = 4;
//    self.payUsingCitrusCardButton.layer.cornerRadius = 4;
//    self.performDPButton.layer.cornerRadius = 4;
    self.containerView.layer.cornerRadius = 4;
    [self.pickerView setHidden:TRUE];
    self.couponCodeTextField.hidden = TRUE;
    self.alteredAmountTextField.hidden = TRUE;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [self.transparentViewView addGestureRecognizer:tapRecognizer];
    
    // Added right Bar button
    UIBarButtonItem *signoutButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Signout"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(signout:)];
    self.navigationItem.rightBarButtonItem = signoutButton;
    
    
    //    UIPickerView *pickerView = [[UIPickerView alloc] init];
    UIToolbar *accessoryToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    // Configure toolbar .....
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(hidePickerView)];
    
    [accessoryToolbar setItems:[NSArray arrayWithObjects:doneButton, nil] animated:YES];
    
    // note: myFirstResponderTableViewCell is an IBOutlet to a static cell in storyboard of type FirstResponderTableViewCell
    self.dynamicPricingTextField.inputView = self.pickerView;
    self.dynamicPricingTextField.inputAccessoryView = accessoryToolbar;
    
}

#pragma mark - Bar Button Methods
- (void) signout:(id)sender{

    [authLayer signOut];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Successfully Signout." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        alertView.tag = 1005;
        [alertView show];
        
    });
}

#pragma mark - Action Methods

// You can get user’s citrus cash balance after you have done Link User.
-(IBAction)getBalance:(id)sender{
    
    self.indicatorView.hidden = FALSE;
    [self.indicatorView startAnimating];
    
    [proifleLayer requetGetBalance:^(CTSAmount *amount, NSError *error) {
        LogTrace(@" value %@ ",amount.value);
        LogTrace(@" currency %@ ",amount.currency);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.indicatorView stopAnimating];
            self.indicatorView.hidden = TRUE;
        });
        
        if (error) {
            [UIUtility toastMessageOnScreen:[error localizedDescription]];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
            self.amountLabel.text = [NSString stringWithFormat:@"%@ %@",amount.currency,amount.value];
            });
//            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Balance is %@ %@",amount.value,amount.currency]];
        }
    }];
}

// make payment using Citrus cash account
-(IBAction)payUsingCitrusCash:(id)sender{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
       UIAlertView *citrusPayAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter amount." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" , nil];
        citrusPayAlert.tag = 1007;
        citrusPayAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField * alertTextField = [citrusPayAlert textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeDecimalPad;
        alertTextField.placeholder = @"Amount";
        [citrusPayAlert show];
    });
}

//Pay money from Cards
-(IBAction)payMoney:(id)sender{

    option = 1;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter amount." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" , nil];
        alert.tag = 1006;
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeDecimalPad;
        alertTextField.placeholder = @"Amount";
        [alert show];
    });
    

}

//Load money to Wallet
-(IBAction)loadMoney:(id)sender{

    option = 0;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter amount." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" , nil];
        alert.tag = 1006;
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField * alertTextField = [alert textFieldAtIndex:0];
        alertTextField.keyboardType = UIKeyboardTypeDecimalPad;
        alertTextField.placeholder = @"Amount";
        [alert show];
    });

}

//Send money to particular user
-(IBAction)sendMoney:(id)sender{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *sendMoneyAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter details." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" , nil];
        sendMoneyAlert.tag = 1008;
        sendMoneyAlert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        UITextField * alertTextField1 = [sendMoneyAlert textFieldAtIndex:0];
        alertTextField1.keyboardType = UIKeyboardTypePhonePad;
        alertTextField1.placeholder = @"User's Phone Number";
        UITextField * alertTextField2 = [sendMoneyAlert textFieldAtIndex:1];
        alertTextField2.keyboardType = UIKeyboardTypeDecimalPad;
        [alertTextField2  setSecureTextEntry:FALSE];
        alertTextField2.placeholder = @"Amount";
        [sendMoneyAlert show];
    });
    
}


-(void)performDynamicPricing:(id)sender{
    self.transparentViewView.frame = frame;
    [UIView animateWithDuration:0.7 animations:^{
        self.transparentViewView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        self.couponCodeTextField.hidden = TRUE;
        self.alteredAmountTextField.hidden = TRUE;
    }];

}

-(void)hideKeyboard:(id)sender{

    [self.view endEditing:YES];

}

-(IBAction)hideDynamicPricingView:(id)sender{
    
//    [UIView animateWithDuration:0.7 animations:^{
//        self.transparentViewView.frame =  CGRectMake(0, self.view.bounds.size.height+100, self.view.bounds.size.width, self.view.bounds.size.height);;
////
//    }];
    [self resetUI];
    [self.view endEditing:YES];
   
    
}

-(IBAction)applyCodeAction:(id)sender{
    self.transparentViewView.hidden = TRUE;
    
    option = 2;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self performSegueWithIdentifier:@"CardViewIdentifier" sender:self];
    });
    
}

- (void)hidePickerView{
    
    [self.dynamicPricingTextField resignFirstResponder];
}

#pragma mark - Class Level Methods
+ (CTSBill*)getBillFromServer:(NSString *)amount{
    // Configure your request here.

     NSString *billURL = [NSString stringWithFormat:@"%@?amount=%@",BillUrl,amount];
   
    
    NSMutableURLRequest* urlReq = [[NSMutableURLRequest alloc] initWithURL:
                                   [NSURL URLWithString:billURL]];
    [urlReq setHTTPMethod:@"POST"];
    NSError* error = nil;

    NSData* signatureData = [NSURLConnection sendSynchronousRequest:urlReq
                                                  returningResponse:nil
                                                              error:&error];
    
    NSString* billJson = [[NSString alloc] initWithData:signatureData
                                               encoding:NSUTF8StringEncoding];
    JSONModelError *jsonError;
    CTSBill* sampleBill = [[CTSBill alloc] initWithString:billJson
                                                    error:&jsonError];
    LogTrace(@"billJson %@",billJson);
    LogTrace(@"signature %@ ", sampleBill);
    return sampleBill;
}

#pragma mark - AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.view endEditing:YES];
    
    if (alertView.tag ==1005){
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if (alertView.tag==1006) {
        
        if (buttonIndex==1) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSegueWithIdentifier:@"CardViewIdentifier" sender:self];
            });
        }
    }
    else if (alertView.tag==1007){
        
        if (buttonIndex==1) {
            
            [self.indicatorView startAnimating];
            self.indicatorView.hidden = FALSE;
            
            // Get Bill
            UITextField * alertTextField = [alertView textFieldAtIndex:0];
            CTSBill *bill = [HomeViewController getBillFromServer:alertTextField.text];
            
            [paymentLayer requestChargeCitrusCashWithContact:contactInfo withAddress:addressInfo  bill:bill customParams:customParams returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *paymentInfo, NSError *error) {
                
                LogTrace(@"paymentInfo %@",paymentInfo);
                LogTrace(@"error %@",error);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.indicatorView stopAnimating];
                    self.indicatorView.hidden = TRUE;
                    
                });
                if(error){
                    [UIUtility toastMessageOnScreen:[error localizedDescription]];
                }
                else{
                    
                    LogTrace(@" isAnyoneSignedIn %d",[authLayer isAnyoneSignedIn]);
                    
                    [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" transaction complete\n txStatus: %@",[paymentInfo.responseDict valueForKey:@"TxStatus"] ]];
                    [self getBalance:nil];
                }
            }];
        }
    }
    else if (alertView.tag==1008) {
        
        if (buttonIndex==1) {
            
            [self.indicatorView startAnimating];
            self.indicatorView.hidden = FALSE;
            
            [paymentLayer requestTransferMoneyTo:[alertView textFieldAtIndex:0].text amount:[alertView textFieldAtIndex:1].text message:@"Here is Some Money for you" completionHandler:^(CTSTransferMoneyResponse*transferMoneyRes,  NSError *error) {
                LogTrace(@" transferMoneyRes %@ ",transferMoneyRes);
                
                LogTrace(@" error %@ ",[error localizedDescription]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.indicatorView stopAnimating];
                    self.indicatorView.hidden = TRUE;
                });
                
                if (error) {
                    
                    [UIUtility toastMessageOnScreen:[error localizedDescription]];
                }
                else{
                    [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Transfer Status %@",transferMoneyRes.status]];
                    [self getBalance:nil];
                }
            }];

        }
    }
    
}


#pragma mark - PickerView Delegate Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [array count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.couponCodeTextField.hidden = TRUE;
    self.alteredAmountTextField.hidden = TRUE;
    self.dynamicPricingTextField.text = [array objectAtIndex:row];
    if ([self.dynamicPricingTextField.text isEqualToString:[array objectAtIndex:1]]) {
        self.couponCodeTextField.hidden = FALSE;
    }
    else if ([self.dynamicPricingTextField.text isEqualToString:[array objectAtIndex:2]]) {
        self.couponCodeTextField.hidden = FALSE;
        self.alteredAmountTextField.hidden = FALSE;
    }
    
    selectedRule =(int) row;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return [array objectAtIndex:row];
    
}


#pragma mark - CollectionView Delegate Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 6;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.layer.cornerRadius = 4;
    
    if (indexPath.row==0) {
        ((UILabel *) [cell.contentView viewWithTag:500]).text = @"Get Balance";
    }
    else if (indexPath.row==1) {
        ((UILabel *) [cell.contentView viewWithTag:500]).text = @"Load Money";
    }
    else if (indexPath.row==2) {
        ((UILabel *) [cell.contentView viewWithTag:500]).text = @"Pay Using Citrus Cash";
    }
    else if (indexPath.row==3) {
        ((UILabel *) [cell.contentView viewWithTag:500]).text = @"Send Money";
    }
    else if (indexPath.row==4) {
        ((UILabel *) [cell.contentView viewWithTag:500]).text = @"Pay Money";
    }
    else if (indexPath.row==5) {
        ((UILabel *) [cell.contentView viewWithTag:500]).text = @"Perform Dynamic Pricing";
    }
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0) {
        self.indicatorView.hidden = FALSE;
        [self.indicatorView startAnimating];
        
        [proifleLayer requetGetBalance:^(CTSAmount *amount, NSError *error) {
            LogTrace(@" value %@ ",amount.value);
            LogTrace(@" currency %@ ",amount.currency);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.indicatorView stopAnimating];
                self.indicatorView.hidden = TRUE;
            });
            
            if (error) {
                [UIUtility toastMessageOnScreen:[error localizedDescription]];
            }
            else{
                
                [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Balance is %@ %@",amount.value,amount.currency]];
            }
        }];
    }
    else if (indexPath.row==1) {
        [self loadMoney:nil];
    }
    else if (indexPath.row==2) {
        [self payUsingCitrusCash:nil];
    }
    else if (indexPath.row==3) {
        [self sendMoney:nil];
    }
    else if (indexPath.row==4) {
        [self payMoney:nil];
    }
    else if (indexPath.row==5) {
        self.transparentViewView.hidden = FALSE;
        [self performDynamicPricing:nil];
    }
    
}

#pragma mark - StoryBoard Delegate Methods
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"CardViewIdentifier"]) {
        
        CardsViewController *viewController = (CardsViewController *)segue.destinationViewController;
        
        //set the Value
        //option = 0 -> Load Money
        //option = 1 -> Pay Money
        //option = 2 -> Perform DP
        
         viewController.landingScreen=option;
        
        if (option==2) {
            
           // dynamicPricingTextField
            //couponCodeTextField;
           // alteredAmountTextField
            CTSRuleInfo *ruleInfo = [[CTSRuleInfo alloc] init];
            ruleInfo.ruleName = self.couponCodeTextField.text;
            ruleInfo.alteredAmount = self.alteredAmountTextField.text;
            viewController.amount = self.amountTextField.text;
            if (selectedRule==0) {
                viewController.dpType = DPRequestTypeSearchAndApply;
            }
            else if (selectedRule==1) {
                    viewController.dpType = DPRequestTypeCalculate;
            }
            else if (selectedRule==2) {
                viewController.dpType = DPRequestTypeValidate;
            }
            
            viewController.ruleInfo = ruleInfo;
            
        }
        else
            viewController.amount = ((UITextField *)[alert textFieldAtIndex:0]).text; //Passing the amount to Card payment screen
        
    }
    
}
- (void)resetUI{

    self.dynamicPricingTextField.text = @"";
    self.amountTextField.text = @"";
    self.couponCodeTextField.text = @"";
    self.alteredAmountTextField.text = @"";
    self.transparentViewView.hidden = TRUE;
//    [UIView animateWithDuration:0.7 animations:^{
//        self.transparentViewView.hidden = TRUE;
//    }];
}


#pragma mark - Dealloc Methods
- (void) dealloc{
    
    self.cancelButton = nil;
    self.applyButton = nil;
//    self.getAmountButton = nil;
//    self.sendMoneyButton = nil;
//    self.payUsingCitrusCardButton = nil;
    self.indicatorView = nil;
    self.amountLabel = nil;
    self.pickerView = nil;
    self.dynamicPricingTextField = nil;
    self.amountTextField = nil;
    self.couponCodeTextField = nil;
    self.alteredAmountTextField = nil;
    self.transparentViewView = nil;
    self.containerView = nil;
    self.dynamicPricingTextField = nil;
    self.amountTextField = nil;
    self.couponCodeTextField = nil;
    self.alteredAmountTextField = nil;
    self.pickerView = nil;
    self.transparentViewView = nil;
    self.containerView = nil;
    self.userName = nil;

}

#pragma mark - TextView Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==self.dynamicPricingTextField) {
        [self.pickerView setHidden:FALSE];
//        self.dynamicPricingTextField.inputView = self.pickerView;
        [self.pickerView reloadAllComponents];
         [self.pickerView selectRow:0 inComponent:0 animated:YES];
        [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
        [self.pickerView removeFromSuperview];
        [self.dynamicPricingTextField becomeFirstResponder];
    }
}



@end
