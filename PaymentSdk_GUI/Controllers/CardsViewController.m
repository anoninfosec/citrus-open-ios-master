//
//  CardsViewController.m
//  PaymentSdk_GUI
//
//  Created by Vikas Singh on 8/26/15.
//  Copyright (c) 2015 Vikas Singh. All rights reserved.
//

#import "CardsViewController.h"
#import "UIUtility.h"

@interface CardsViewController (){

    NSArray *array;
    UITextField *currentTextField;
    NSDictionary *codeDict;
    UISegmentedControl *segControl;
    CTSPaymentDetailUpdate *cardInfo;
    NSArray *debitArray;
    NSArray *creditArray;
    NSMutableArray *saveCardsArray;
    NSDictionary *netBankingDict;
    NSInteger selectedRow;
//    UIView *contentView;
}

@end

@implementation CardsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self initialSetting];
    
}

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

#pragma mark - Initial Setting Methods
- (void) initialSetting{
    
//    [self dynamicPricing];
    
    
    UIFont *font = [UIFont systemFontOfSize:11.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [self.segmentedControl setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    

    self.indicatorView.hidden = TRUE;
    self.loadButton.layer.cornerRadius = 4;
    self.loadMoneyButton.layer.cornerRadius = 4;
    self.cardView.layer.cornerRadius = 4;
    self.netBankingView.layer.cornerRadius = 4;
    self.schemeTypeImageView.layer.cornerRadius = 4;
    
    [self.pickerView setHidden:TRUE];
    [self.saveCardsTableView setHidden:TRUE];
    self.netBankingView.hidden = TRUE;
    array =[[NSArray alloc]init];
    saveCardsArray =[[NSMutableArray alloc]init];
    
    if (self.landingScreen==1) {
        
        self.title = @"Payment";
        [self.loadButton setTitle:@"Payment" forState:UIControlStateNormal];
        [self.loadMoneyButton setTitle:@"Payment" forState:UIControlStateNormal];
    }
    else if (self.landingScreen==0){
        self.title = @"Load Money";
        [self.loadButton setTitle:@"Load Money" forState:UIControlStateNormal];
        [self.loadMoneyButton setTitle:@"Load Money" forState:UIControlStateNormal];
    }
    else if (self.landingScreen==2){
        self.title = @"Dynamic Pricing";
        [self.loadButton setTitle:@"Apply Dynamic Pricing" forState:UIControlStateNormal];
        [self.loadMoneyButton setTitle:@"Apply Dynamic Pricing" forState:UIControlStateNormal];
    }
    
    
    [self requestPaymentModes];
    
//    codeDict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"CID002", @"AXIS Bank",
//                @"CID003", @"CITY Bank",
//                @"CID010", @"HDFC Bank",
//                @"CID001", @"ICICI Bank",
//                @"CID033",@"KOTAK Bank",
//                @"CID005", @"SBI Bank",
//                @"CID007", @"UNION Bank",
//                @"CID004", @"YES Bank",
//                @"CID011", @"IDBI Bank",
//                @"CID042",@"VIJAYA Bank",
//                @"CID070",@"UCO Bank",
//                @"CID053",@"Cosmos Bank",
//                @"CID041",@"United Bank of India",
//                @"CID044",@"PNB Retail",
//                @"CID031",@"Karnataka Bank",
//                @"CID028",@"Induslnd Bank",
//                @"CID009",@"Federal Bank",
//                @"CID021",@"Bank of Maharashtra",
//                @"CID051",@"Canara Bank",
//                @"CID045",@"Catholic Syrian Bank",
//                @"CID023",@"Central Bank of India",
//                @"CID008",@"Indian Bank",
//                @"CID027",@"Indian Overseas Bank",
//                @"CID029",@"ING VYSA",
//                @"CID032",@"Karur Vysya Bank",
//                nil];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard:)];
    [self.cardView addGestureRecognizer:tapRecognizer];
    
    
//    UIPickerView *pickerView = [[UIPickerView alloc] init];
    UIToolbar *accessoryToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    // Configure toolbar .....
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(hidePickerView)];
    
    [accessoryToolbar setItems:[NSArray arrayWithObjects:doneButton, nil] animated:YES];
    
    // note: myFirstResponderTableViewCell is an IBOutlet to a static cell in storyboard of type FirstResponderTableViewCell
    self.netBankCodeTextField.inputView = self.pickerView;
    self.netBankCodeTextField.inputAccessoryView = accessoryToolbar;
    
}

#pragma mark - Action Methods
// You can load/add money as per following way
-(IBAction)loadUsingCard:(id)sender{
    
    segControl = (UISegmentedControl *)sender;
    
     [self.view endEditing:YES];
    [self.saveCardButton setSelected:NO];
    
    [self.saveCardButton setImage:[UIImage imageNamed:@"Unselected"] forState:UIControlStateNormal];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self resetUI];
        
    });
    
    if (segControl.selectedSegmentIndex==0 || segControl.selectedSegmentIndex==1) {

        [self.saveCardsTableView setHidden:TRUE];
        [self.pickerView setHidden:TRUE];
        self.netBankingView.hidden = TRUE;
        self.cardView.hidden = FALSE;
        
    }
    else if (segControl.selectedSegmentIndex==2){
        
        [self.saveCardsTableView setHidden:TRUE];
        self.cardView.hidden = TRUE;
        self.netBankingView.hidden = FALSE;
        
    }
    else if (segControl.selectedSegmentIndex==3){
        self.cardView.hidden = TRUE;
        [self.pickerView setHidden:TRUE];
        self.netBankingView.hidden = TRUE;
        self.saveCardsTableView.hidden = FALSE;
        [self getSaveCards:nil];
    }
}

-(IBAction)saveCard:(UIButton *)sender{

   self.loadMoneyButton.userInteractionEnabled = FALSE;
    
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [sender setImage:[UIImage imageNamed:@"Unselected"] forState:UIControlStateNormal];
        
    }
    else{
        [self setCardInfo];
        [sender setSelected:YES];
        [sender setImage:[UIImage imageNamed:@"Selected"] forState:UIControlStateNormal];
        // Configure your request here.
        [proifleLayer updatePaymentInformation:cardInfo withCompletionHandler:^(NSError *error) {
            self.loadMoneyButton.userInteractionEnabled = TRUE;
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setSelected:NO];
                [self.saveCardButton setImage:[UIImage imageNamed:@"Unselected"] forState:UIControlStateNormal];
            });
            if(error == nil){
                // Your code to handle success.
                [UIUtility toastMessageOnScreen:@" successfully card saved "];
            }
            else {
                // Your code to handle error.
                [UIUtility toastMessageOnScreen:@"Couldn't save card\n All fields are mandatory."];
//                [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@" couldn't save card\n error: %@",error]];
            }
        }];
    }
}

-(IBAction)loadMoney:(id)sender{

    [self.view endEditing:YES];
    // Credit card
//    self.indicatorView.hidden = FALSE;
//    [self.indicatorView startAnimating];
    
    NSString *cardNumber = [self.cardNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (segControl.selectedSegmentIndex==0) {
        BOOL isSchemeAvailable = FALSE;
        for(NSString *string in debitArray){
            if ([string caseInsensitiveCompare:[CTSUtility fetchCardSchemeForCardNumber:cardNumber]] == NSOrderedSame) {
                isSchemeAvailable=TRUE;
                break;
            }
        }
        if (!isSchemeAvailable) {
        
            [UIUtility toastMessageOnScreen:@"This card scheme is not valid for you.Please Contact to Citruspay care."];
            return;
        }
    }
    else if (segControl.selectedSegmentIndex==1){
        BOOL isSchemeAvailable = FALSE;
        for(NSString *string in creditArray){
            if ([string caseInsensitiveCompare:[CTSUtility fetchCardSchemeForCardNumber:cardNumber]] == NSOrderedSame) {
                isSchemeAvailable = TRUE;
                break;
            }
        }
        if (!isSchemeAvailable) {
            
            [UIUtility toastMessageOnScreen:@"This card scheme is not valid for you.Please Contact to Citruspay care."];
            return;
        }
    }
 
        
    [self setCardInfo];
    
    if (self.landingScreen==1) {
        
        // Get your bill here.
        CTSBill *bill = [CardsViewController getBillFromServer:self.amount];
        
        [paymentLayer requestChargePayment:cardInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:nil returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
            /*
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.indicatorView stopAnimating];
                self.indicatorView.hidden = TRUE;
            });*/
            if(error){
                [UIUtility toastMessageOnScreen:error.localizedDescription];
            }
            else {
                [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
                [self resetUI];
            }
        }];
    }
    else if(self.landingScreen==0){
        [paymentLayer requestLoadMoneyInCitrusPay:cardInfo withContact:contactInfo withAddress:addressInfo amount:self.amount returnUrl:ReturnUrl customParams:customParams  returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
            /*
             dispatch_async(dispatch_get_main_queue(), ^{
             [self.indicatorView stopAnimating];
             self.indicatorView.hidden = TRUE;
             });*/
            if(error){
                [UIUtility toastMessageOnScreen:error.localizedDescription];
            }
            else {
                [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Load Money Status %@",[citrusCashResponse.responseDict valueForKey:LoadMoneyResponeKey]]];
                [self resetUI];
            }
        }];
    }
    else if(self.landingScreen==2){
    
        [self dynamicPricing];
    
    }
    
}

- (IBAction)getSaveCards:(id)sender{
    
     self.indicatorView.hidden = FALSE;
    [self.indicatorView startAnimating];
   
    // Get the bind user cards.
    // Configure your request here.
    [proifleLayer requestPaymentInformationWithCompletionHandler:^(CTSProfilePaymentRes *paymentInfo, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.indicatorView stopAnimating];
            self.indicatorView.hidden = TRUE;

        });
        if (error == nil) {
            // Your code to handle success.
            NSMutableString *toastString = [[NSMutableString alloc] init];
            if([paymentInfo.paymentOptions count])
            {
                saveCardsArray = (NSMutableArray *) paymentInfo.paymentOptions;
               // sleep(3);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.saveCardsTableView reloadData];
                });
                
                
                //   [toastString appendString:[self convertToString:[paymentInfo.paymentOptions objectAtIndex:0]]];
                
            }
            else{
                toastString =(NSMutableString *) @"No saved cards, please save card first";
                [UIUtility toastMessageOnScreen:toastString];
            }
            
        } else {
            // Your code to handle error.
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Couldn't find saved cards \nerror: %@",[error localizedDescription]]];
        }
    }];
}

// Tokenized card payment.
-(IBAction)tokenizedPaymentWithToken:(NSString *)token andCVV:(NSString *)cvv{
    
    CTSPaymentDetailUpdate *tokenizedCardInfo = [[CTSPaymentDetailUpdate alloc] init];
    // Update card for tokenized payment.
    CTSElectronicCardUpdate *tokenizedCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
    tokenizedCard.cvv= cvv;
    tokenizedCard.token= token; // @"5115669e6129247a1e7a3599ea58e947";
    [tokenizedCardInfo addCard:tokenizedCard];
    //
    // Get your bill here.
    CTSBill *bill = [CardsViewController getBillFromServer:self.amount];
    
    [paymentLayer requestChargeTokenizedPayment:tokenizedCardInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:customParams returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.indicatorView stopAnimating];
            self.indicatorView.hidden = TRUE;
        });
        if(error){
            [UIUtility toastMessageOnScreen:error.localizedDescription];
        }
        else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
        }
    }];
    
}


- (void)resignKeyboard:(UITapGestureRecognizer *)sender{
    
    [self.view endEditing:YES];
}

- (void)hidePickerView{

    [currentTextField resignFirstResponder];
}



-(void)dynamicPricing{
    
//    //Citrus cash
//    CTSPaymentDetailUpdate *paymentInfo = [[CTSPaymentDetailUpdate alloc] initCitrusCash];
    
    
    CTSUser *user = [[CTSUser alloc] init];
    user.email = @"vikas.singh@citruspay.com";
    user.mobile = @"9533998688";
    
    CTSBill *bill = [CardsViewController getDPBillFromServer:self.amount operation:self.dpType ruleInfo:self.ruleInfo];
    
    [paymentLayer requestPerformDynamicPricingRule:self.ruleInfo paymentInfo:cardInfo bill:bill user:user type:self.dpType extraParams:nil completionHandler:^(CTSDyPResponse *dyPResponse, NSError *error) {
        LogTrace(@"error %@",error);
        LogTrace(@"dyPResponse %@",dyPResponse);
        if (error==nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Want to Proceed?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Pay" , nil];
            alert.tag = 101;
            
            [alert show];
        }
        else
             [UIUtility toastMessageOnScreen:error.localizedDescription];
    }];
}

-(void)payUsingDp{
    
    [paymentLayer requestChargeDynamicPricingContact:contactInfo withAddress:addressInfo customParams:nil returnViewController:self withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
        NSLog(@"citrusCashResponse %@",citrusCashResponse);
        NSLog(@"error %@",error);
    }];
}

- (void)doneClicked:(id)sender{
    
    currentTextField.text = [array objectAtIndex:2];
    [currentTextField resignFirstResponder];
    
}

#pragma mark - TextView Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{

    if (textField==self.netBankCodeTextField) {
        [self.pickerView setHidden:FALSE];
        
        currentTextField=textField;
        array = [netBankingDict allKeys];
//        array = [codeDict allKeys];
//        self.netBankCodeTextField.inputView = self.pickerView;

        [self.pickerView reloadAllComponents];
        [self.pickerView selectRow:0 inComponent:0 animated:YES];
        [self pickerView:self.pickerView didSelectRow:0 inComponent:0];
        [self.pickerView removeFromSuperview];
        [self.netBankCodeTextField becomeFirstResponder];
    }
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == self.cardNumberTextField) {
        __block NSString *text = [textField text];
        if ([textField.text isEqualToString:@""] || ( [string isEqualToString:@""] && textField.text.length==1)) {
            self.schemeTypeImageView.image = [CTSUtility getSchmeTypeImage:string];
        }
        
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (text.length>1) {
            self.schemeTypeImageView.image = [CTSUtility getSchmeTypeImage:text];
        }
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        if (newString.length>1) {
            NSString* scheme = [CTSUtility fetchCardSchemeForCardNumber:[newString stringByReplacingOccurrencesOfString:@" " withString:@""]];
            if ([scheme isEqualToString:@"MTRO"]) {
                if (newString.length >= 24) {
                    return NO;
                }
            }
            else{
                if (newString.length >= 20) {
                    return NO;
                }
            }
        }
        
        [textField setText:newString];
        return NO;
        
    }
//    if (textField==self.expiryDateTextField) {
//         __block NSString *text = [textField text];
//        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789/"];
//        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
//        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
//            return NO;
//        }
//        
//        text = [text stringByReplacingCharactersInRange:range withString:string];
//        text = [text stringByReplacingOccurrencesOfString:@"/" withString:@""];
//        
//        NSString *newString = @"";
//        while (text.length > 0) {
//            NSString *subString = [text substringToIndex:MIN(text.length, 2)];
//            newString = [newString stringByAppendingString:subString];
//            if (subString.length == 2) {
//                newString = [newString stringByAppendingString:@"/"];
//            }
//            text = [text substringFromIndex:MIN(text.length, 2)];
//        }
//        
//        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
//        
//        if (newString.length >=5) {
//            return NO;
//        }
//        
//        [textField setText:newString];
//        return NO;
//    }
    
    return YES;
   
}

#pragma mark - PickerView Delegate Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
        return [array count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {

        return 1;

}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    currentTextField.text = [array objectAtIndex:row];
//    [currentTextField resignFirstResponder];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

        return [array objectAtIndex:row];
    
}

#pragma mark - TableView Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return saveCardsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"saveCardIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    [cell.contentView viewWithTag:1000].layer.cornerRadius = 5;
    NSDictionary *tempDict = [saveCardsArray objectAtIndex:indexPath.row];
    ((UILabel *) [cell.contentView viewWithTag:1001]).text = [tempDict valueForKey:@"name"];
    ((UILabel *) [cell.contentView viewWithTag:1002]).text = [tempDict valueForKey:@"number"];
    ((UILabel *) [cell.contentView viewWithTag:1003]).text = [tempDict valueForKey:@"owner"];
    NSMutableString *string = [[tempDict valueForKey:@"expiryDate"] mutableCopy];
    [string insertString:@"/" atIndex:2];
    ((UILabel *) [cell.contentView viewWithTag:1004]).text = string;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        ((UIImageView *) [cell.contentView viewWithTag:1005]).image = [UIImage imageNamed:((NSString *)[tempDict valueForKey:@"scheme"]).lowercaseString];
        
    });
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    selectedRow = indexPath.row;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *cvvAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter cvv." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok" , nil];
        cvvAlert.tag = 100;
        cvvAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        UITextField * cvvTextField = [cvvAlert textFieldAtIndex:0];
        cvvTextField.keyboardType = UIKeyboardTypeNumberPad;
        cvvTextField.placeholder = @"cvv";
        
        [cvvAlert show];
    });
    
}

#pragma mark - Class Level Methods
+ (CTSBill*)getBillFromServer:(NSString *)amount{
    
    // Configure your request here.
    NSString *billURL = [NSString stringWithFormat:@"%@?amount=%@",BillUrl,amount];
   
    NSMutableURLRequest* urlReq = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:billURL]];
    [urlReq setHTTPMethod:@"POST"];
    NSError* error = nil;
    NSData* signatureData = [NSURLConnection sendSynchronousRequest:urlReq
                                                  returningResponse:nil
                                                              error:&error];
    
    NSString* billJson = [[NSString alloc] initWithData:signatureData
                                               encoding:NSUTF8StringEncoding];
    JSONModelError *jsonError;
    CTSBill* sampleBill = [[CTSBill alloc] initWithString:billJson
                                                    error:&jsonError];
    LogTrace(@"billJson %@",billJson);
    LogTrace(@"signature %@ ", sampleBill);
    return sampleBill;
}

+ (CTSBill*)getDPBillFromServer:(NSString *)amount operation:(DPRequestType)type ruleInfo:(CTSRuleInfo *)ruleInfo{
    // Configure your request here.
    NSMutableURLRequest* urlReq;
    
    switch (type) {
        case DPRequestTypeValidate:{
            urlReq = [[NSMutableURLRequest alloc] initWithURL:
                                           [NSURL URLWithString:[NSString stringWithFormat:@"%@?amount=%@&dpOperation=%@&ruleName=%@&alteredAmount=%@",BillUrl,amount,[CardsViewController toDpTypeString:type],ruleInfo.ruleName,ruleInfo.alteredAmount]]];
        }
            break;
        case DPRequestTypeSearchAndApply:{
            urlReq = [[NSMutableURLRequest alloc] initWithURL:
                      [NSURL URLWithString:[NSString stringWithFormat:@"%@?amount=%@&dpOperation=%@",BillUrl,amount,[CardsViewController toDpTypeString:type]]]];
        }
            break;
        case DPRequestTypeCalculate:{
            urlReq = [[NSMutableURLRequest alloc] initWithURL:
                      [NSURL URLWithString:[NSString stringWithFormat:@"%@?amount=%@&dpOperation=%@&ruleName=%@",BillUrl,amount,[CardsViewController toDpTypeString:type],ruleInfo.ruleName]]];
        }
            break;
        default:
            break;
    }
   
    
    [urlReq setHTTPMethod:@"POST"];
    NSError* error = nil;
    NSData* signatureData = [NSURLConnection sendSynchronousRequest:urlReq
                                                  returningResponse:nil
                                                              error:&error];
    NSString* billJson = [[NSString alloc] initWithData:signatureData
                                               encoding:NSUTF8StringEncoding];
    JSONModelError *jsonError;
    CTSBill* sampleBill = [[CTSBill alloc] initWithString:billJson
                                                    error:&jsonError];
    LogTrace(@"billJson %@",billJson);
    LogTrace(@"signature %@ ", sampleBill);
    //sampleBill.dpSignature = @"f07abf0a768e9f3ee9bed9c0f1eba1c0a2d88db7";
    //sampleBill.merchantTxnId = @"144119346346509";
    return sampleBill;
}

+(NSString *)toDpTypeString:(DPRequestType)type{
    
    NSString *typeString = nil;
    switch (type) {
        case DPRequestTypeValidate:
            typeString = @"validateRule";
            break;
        case DPRequestTypeSearchAndApply:
            typeString = @"searchAndApply";
            break;
        case DPRequestTypeCalculate:
            typeString = @"calculatePricing";
            break;
        default:
            break;
    }
    
    return typeString;
}


-(void)requestPaymentModes{
    [paymentLayer requestMerchantPgSettings:VanityUrl withCompletionHandler:^(CTSPgSettings *pgSettings, NSError *error) {
        if(error){
            //handle error
            LogTrace(@"[error localizedDescription] %@ ", [error localizedDescription]);
        }
        else {
            
            debitArray = [CTSUtility fetchMappedCardSchemeForSaveCards:[[NSSet setWithArray:pgSettings.debitCard] allObjects] ];
            creditArray = [CTSUtility fetchMappedCardSchemeForSaveCards:[[NSSet setWithArray:pgSettings.creditCard] allObjects] ];
            
//            debitArray = [CTSUtility fetchMappedCardSchemeForSaveCards:pgSettings.debitCard ];
//            creditArray = [CTSUtility fetchMappedCardSchemeForSaveCards:pgSettings.creditCard ];
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
            netBankingDict = tempDict;
            
            LogTrace(@" pgSettings %@ ", pgSettings);
            for (NSString* val in creditArray) {
                LogTrace(@"CC %@ ", val);
            }
            
            for (NSString* val in debitArray) {
                LogTrace(@"DC %@ ", val);
            }
            
            for (NSDictionary* arr in pgSettings.netBanking) {
                [tempDict setObject:[arr valueForKey:@"issuerCode"] forKey:[arr valueForKey:@"bankName"]];
                LogTrace(@"bankName %@ ", [arr valueForKey:@"bankName"]);
                LogTrace(@"issuerCode %@ ", [arr valueForKey:@"issuerCode"]);
                
            }
            
        }
    }];
}

- (void) setCardInfo{

    CTSPaymentDetailUpdate *tempCardInfo = [[CTSPaymentDetailUpdate alloc] init];
    NSString *cardNumber = [self.cardNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (segControl.selectedSegmentIndex==0) {
        
        // Update card for card payment.
        CTSElectronicCardUpdate *debitCard = [[CTSElectronicCardUpdate alloc] initDebitCard];
        debitCard.number = cardNumber;
        debitCard.expiryDate = [NSString stringWithFormat:@"%@/%@",self.expiryMonthTextField.text , self.expiryYearTextField.text];
        debitCard.scheme =  [CTSUtility fetchCardSchemeForCardNumber:debitCard.number];
        debitCard.ownerName = self.ownerNameTextField.text;
        debitCard.cvv = self.cvvTextField.text;
//        debitCard.name = @"Kotak";
        [tempCardInfo addCard:debitCard];
        
    }
    else if (segControl.selectedSegmentIndex==1) {
        
        // Update card for card payment.
        CTSElectronicCardUpdate *creditCard = [[CTSElectronicCardUpdate alloc] initCreditCard];
        creditCard.number = cardNumber;
        creditCard.expiryDate = [NSString stringWithFormat:@"%@/%@",self.expiryMonthTextField.text,self.expiryYearTextField.text];
        creditCard.scheme =  [CTSUtility fetchCardSchemeForCardNumber:creditCard.number];
        creditCard.ownerName = self.ownerNameTextField.text;
        creditCard.cvv = self.cvvTextField.text;
        [tempCardInfo addCard:creditCard];
    }
    else{
        // Update bank details for net banking payment.
        CTSNetBankingUpdate* netBank = [[CTSNetBankingUpdate alloc] init];
        NSString *code = [netBankingDict valueForKey:self.netBankCodeTextField.text];
//        NSString *code = [codeDict valueForKey:self.netBankCodeTextField.text];
        netBank.code = code;
        [tempCardInfo addNetBanking:netBank];
        
    }
    cardInfo = nil;
    cardInfo = tempCardInfo;

}

#pragma mark - AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.view endEditing:YES];
    
    if (alertView.tag==100){
        
        if (buttonIndex==1) {
            //            [self.indicatorView startAnimating];
            //            self.indicatorView.hidden = FALSE;
            
            UITextField * alertTextField = [alertView textFieldAtIndex:0];
            [alertTextField resignFirstResponder];
            NSDictionary *dict =[saveCardsArray objectAtIndex:selectedRow];
            NSString *token =[dict valueForKey:@"token"];
//            dispatch_async(dispatch_get_main_queue(), ^{
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self tokenizedPaymentWithToken:token andCVV:alertTextField.text];
            });
            
        }
    }
    else if (alertView.tag==101){
        
        if (buttonIndex==1) {
            
            //            dispatch_async(dispatch_get_main_queue(), ^{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self payUsingDp];
            });
            
        }
    }
    
}


#pragma mark - Reset UI Methods
- (void) resetUI{
    
    self.cardNumberTextField.text = @"";
    self.ownerNameTextField.text = @"";
    self.expiryMonthTextField.text = @"";
    self.expiryYearTextField.text = @"";
    self.schemeTextField.text = @"";
    self.cvvTextField.text = @"";
    self.netBankCodeTextField.text = @"";
    self.schemeTypeImageView.image = nil;
    
}

#pragma mark - Dealloc Methods
- (void) dealloc{
    
    self.cardNumberTextField = nil;
    self.ownerNameTextField = nil;
    self.expiryMonthTextField = nil;
    self.expiryYearTextField = nil;
    self.schemeTextField = nil;
    self.cvvTextField = nil;
    self.netBankCodeTextField = nil;
    self.loadButton = nil;
    self.loadMoneyButton = nil;
    self.netBankingView = nil;
    self.cardView = nil;
    self.pickerView = nil;
    self.indicatorView = nil;
    self.schemeTypeImageView = nil;
  
}


@end
